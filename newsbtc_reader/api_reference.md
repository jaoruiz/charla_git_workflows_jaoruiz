#Emdyp API for news - Documentation files

#1 GENERAL

Emdyp API for news is a json api that shows a collection of data about 
the last news of newsbtc franchises to people who wants to consult
newsbtc feed on their webpages.

#2 STRUCTURE AND CALLING METHOD

Emdyp API for news uses rest api format (url as resource), the url is built
as the concatenation of one root api url and the franchise subdomain. There are
franchises we're currently supporting and other that are not currently being
supported, so, franchises currently suported are:

* ph(philiphines)
* lta(latin-america)
* ca(canada)
* www or global(global)
* uk(united kingdom)
* us(united states)
* af(africa)

The url to consult the information about the last

* root api url(for now): http://api-emdyp.rhcloud.com/news/last
* use: root_api_url + franchise subdomain
* last 10 news of latin-america franchise: http://api-emdyp.rhcloud.com/news/last/lta
* last 10 news of africa franchise: http://api-emdyp.rhcloud.com/news/last/af

only GET method is allowed to be used, the result of GET request is
an array of 10 elements as follows:

    [{"name":"article's title",
      "url": "article's url",
      "thumbnail": "article's thumbnail",
      "author": "aritcle's author",
      "date": "article's publication date",
      "description": "article's sumary"},
     .
     .
     .
    ]

## IMPORTANT INFORMATION RELATED

this information is obtained from http://franchise.newsbtc.com/post-view/ webpage
that's why is needed to provide the franchise subdomain, if subdomain doesnt exist
or is not enabled, the api will return the data for latin-america franchise (lta)

#3 ABOUT US

Emdyp.me developed this software, we are a start-up that works with technology and 
language services, visit us at http://emdyp.me, you can write us at [mailbox@emdyp.me](mailto:mailbox@emdyp.me)
or to [latinamerica@newsbtc.com](mailto:latinamerica@newsbtc.com), or follow us at @emdyp.

every comment, help or donation is welcome, you can support us by helping with the development,
designing cool id templates, giving us your feedback.

###Donations
* bitcoin: 1B3RmdZJbR4ArU8MdKL2UPCuacAnRMMmNe

we support bogota coinfest 2015, even in blockchain :) http://www.cryptograffiti.info/?txnr=2238